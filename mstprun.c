/*
 * mstprun.c: a process to activate the mstp line discipline
 *
 * Since this process keeps running in an embedded system, keep it small
 *
 * ####COPYRIGHTBEGIN####
 * -------------------------------------------
 * Copyright (c) 2004 Coleman Brumley (cbrumley@users.sourceforge.net)
 *
 * Based, in part, on code by:
 * Copyright (c) 2001 Alessandro Rubini (rubini@linux.it)
 * Copyright (c) 2001 CiaoLab S.r.L. (info@ciaolab.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to
 * The Free Software Foundation, Inc.
 * 59 Temple Place - Suite 330
 * Boston, MA  02111-1307, USA.
 *
 * (See the included file COPYING)
 *
 * -------------------------------------------
 * ####COPYRIGHTEND####
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <limits.h>

#include <sys/types.h>
#include <sys/ioctl.h>

#include <signal.h>

#include "mstp_ioctl.h"


#define OPEN_FLAGS		(O_RDWR)

#define TTY_SPEED		B9600
#define TTY_IFLAG		(IGNPAR)
#define TTY_OFLAG		0
#define TTY_CFLAG		(TTY_SPEED | CS8 | CREAD |  CLOCAL)
#define TTY_LFLAG	        0

static int fd = -1; //the one and only file descriptor

int configure(int fd, int toggle)
{
    struct termios newtio;
    unsigned int bits;

    //ioctl(fd, TIOCMGET, &bits); /* get the serial port status */
    //bits |= TIOCM_DTR;		/* set DTR low  */
    //bits &= ~TIOCM_RTS;		/* set RTS high */
    //ioctl(fd, TIOCMSET, &bits); /* get the serial port status */

#if 0
    if (toggle) {
	/* First of all, toggle RTS and DTR */
	if (ioctl(fd,TIOCMGET,&bits)) return -1;
	bits &= ~(TIOCM_RTS | TIOCM_DTR);
	if (ioctl(fd,TIOCMSET,&bits)) return -1;
	usleep(300*1000); bits |= (TIOCM_RTS | TIOCM_DTR);
	if (ioctl(fd,TIOCMSET,&bits)) return -1;
	usleep(300*1000);
    }

    fcntl(fd, F_SETFL, 0); /* clear ndelay */

    memset(&newTermIo, 0, sizeof(struct termios));
   
     /* save current port settings */
    tcgetattr ( fd, &newTermIo );

    newTermIo.c_iflag = TTY_IFLAG;
    newTermIo.c_oflag = TTY_OFLAG;
    newTermIo.c_cflag = TTY_CFLAG;
    newTermIo.c_cflag &= ~HUPCL;
    newTermIo.c_lflag = TTY_LFLAG;
    newTermIo.c_cc[VMIN] = 0;
    newTermIo.c_cc[VTIME] = 0;

    if (cfsetispeed(&newTermIo, TTY_SPEED) < 0)
	return -1;
    if (tcsetattr(fd, TCSANOW, &newTermIo) < 0)
	return -1;
    tcflush(fd, TCIOFLUSH);
#endif

    //Perform reads and writes and immediately return.
    fcntl ( fd, F_SETFL, FNDELAY );

    /*Initialize the new port settings*/
    bzero(&newtio, sizeof(newtio));

    //Control setiings - Baudrate, receiver on, non-modem
    newtio.c_cflag = B9600 | CREAD | CLOCAL;

    //clear the HUPCL bit, close doesn't change DTR
    newtio.c_cflag &= ~HUPCL;

    //setup for 8-N-1
    newtio.c_cflag |= CS8;			//8 databits
    newtio.c_cflag &= ~(PARENB | PARODD); 	//No parity
    newtio.c_cflag &= ~CSTOPB; 			//1 stop bit

    /* set input flag non-canonical, no processing */
    newtio.c_lflag = 0;

    /* ignore parity errors */
    newtio.c_iflag = (IGNBRK | IGNPAR);

    /* set output flag non-canonical, no processing */
    newtio.c_oflag = 0;

    newtio.c_cc[VTIME] = 0;   /* no time delay */
    newtio.c_cc[VMIN]  = 0;   /* no char delay */

    /* flush the buffer */
    tcflush(fd, TCIFLUSH);

    /*implement the new port settings after current write*/
    tcsetattr(fd,TCSADRAIN,&newtio);

    ioctl(fd, TIOCMGET, &bits); /* get the serial port status */

    return 0;
}

/* Catch any signals. */
static void sig_catch(int sig)
{
    (void) signal(sig, sig_catch);
    close(fd);
    exit(0);
}

int main(int argc, char **argv)
{
    int i;
    int max_master, max_info_frames, TS;
    int result;

    if (argc != 2) {
	fprintf(stderr, "%s: Use \"%s <dev>\"\n", argv[0], argv[0]);
	exit(1);
    }
    if( (fd=open(argv[1], OPEN_FLAGS)) == -1){
	fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1], strerror(errno));
	exit(1);
    }
    configure(fd,0);

    /* again (otherwise, it won't work after boot) 
    close(fd);
    if ((fd=open(argv[1], OPEN_FLAGS)) == -1){
	fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1], strerror(errno));
	exit(1);
    }
    configure(fd,0);
    */
    

    /* now, switch to the N_MOUSE line discipline and wait forever  */
    i = N_MOUSE;
    ioctl(fd, TIOCSETD, &i);

    printf("Reading via IOCTL\n");
    printf("=================\n");
    max_master = ioctl(fd,MSTP_IOC_GETMAXMASTER); //retrieves the max_master setting
    printf("max_master = %d\n",max_master);
    max_info_frames = ioctl(fd,MSTP_IOC_GETMAXINFOFRAMES); //retrieves the max_info_frames setting
    printf("max_info_frames = %d\n",max_info_frames);
    TS = ioctl(fd,MSTP_IOC_GETMACADDRESS); //retrieves the mac address (TS) setting
    printf("TS = %d\n",TS);
    
    printf("Writing via IOCTL\n");
    printf("=================\n");
    result = ioctl(fd,MSTP_IOC_SETMAXMASTER,127); //sets Nmax_master to 10
    if(result < 0) printf("SetMaxMaster Failed\n");
    result = ioctl(fd,MSTP_IOC_SETMAXINFOFRAMES,1); //sets Nmax_info_frames to 5
    if(result < 0) printf("SetMaxInfoFrames Failed\n");
    result = ioctl(fd,MSTP_IOC_SETMACADDRESS,2); //sets This_Station to 35
    if(result < 0) printf("SetMACAddress Failed\n");
    
    printf("Reading via IOCTL\n");
    printf("=================\n");
    max_master = ioctl(fd,MSTP_IOC_GETMAXMASTER); //retrieves the max_master setting
    printf("max_master = %d\n",max_master);
    max_info_frames = ioctl(fd,MSTP_IOC_GETMAXINFOFRAMES); //retrieves the max_info_frames setting
    printf("max_info_frames = %d\n",max_info_frames);
    TS = ioctl(fd,MSTP_IOC_GETMACADDRESS); //retrieves the mac address (TS) setting
    printf("TS = %d\n",TS);

    (void) signal(SIGHUP, sig_catch);
    (void) signal(SIGINT, sig_catch);
    (void) signal(SIGQUIT, sig_catch);
    (void) signal(SIGTERM, sig_catch);

    while (1) 
	sleep(60);

    exit(0);
}
