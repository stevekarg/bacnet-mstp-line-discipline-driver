
/*
* mstpdump.c: decode the serial port (independently of the mstp module
*
* ####COPYRIGHTBEGIN####
* -------------------------------------------
* Copyright (c) 2004 Coleman Brumley (cbrumley@users.sourceforge.net)
*
* Based, in part, on code by:
* Copyright (c) 2001 Alessandro Rubini (rubini@linux.it)
* Copyright (c) 2001 CiaoLab S.r.L. (info@ciaolab.com)
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* as published by the Free Software Foundation; either version 2.1
* of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this program; if not, write to
* The Free Software Foundation, Inc.
* 59 Temple Place - Suite 330
* Boston, MA  02111-1307, USA.
*
* (See the included file COPYING)
*
* -------------------------------------------
* ####COPYRIGHTEND####
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include <sys/types.h>
#include <sys/ioctl.h>


#define OPEN_FLAGS		(O_RDWR  | O_NOCTTY | O_NONBLOCK)

#define TTY_IFLAG		(IGNBRK  |IGNPAR)
#define TTY_OFLAG		0
#define TTY_CFLAG		(CS8 | CREAD | PARENB | PARODD | CLOCAL)
#define TTY_LFLAG	        0
#define TTY_SPEED		B9600


int configure(int fd, int toggle)
{
    struct termios newTermIo;
    unsigned int bits;

    if (toggle) {
	/* First of all, toggle RTS and DTR */
	if (ioctl(fd,TIOCMGET,&bits)) return -1;
	bits &= ~(TIOCM_RTS | TIOCM_DTR);
	if (ioctl(fd,TIOCMSET,&bits)) return -1;
	usleep(300*1000); bits |= (TIOCM_RTS | TIOCM_DTR);
	if (ioctl(fd,TIOCMSET,&bits)) return -1;
	usleep(300*1000);
    }

    memset(&newTermIo, 0, sizeof(struct termios));
    
    newTermIo.c_iflag = TTY_IFLAG;
    newTermIo.c_oflag = TTY_OFLAG;
    newTermIo.c_cflag = TTY_CFLAG;
    newTermIo.c_lflag = TTY_LFLAG;
    newTermIo.c_cc[VMIN] = 1;
    newTermIo.c_cc[VTIME] = 0;

    if (cfsetispeed(&newTermIo, TTY_SPEED) < 0)
	return -1;
    if (tcsetattr(fd, TCSANOW, &newTermIo) < 0)
	return -1;
    tcflush(fd, TCIOFLUSH);
    fcntl(fd, F_SETFL, 0); /* clear ndelay */

    return 0;
}

int main(int argc, char **argv)
{
    int fd;
    int i,n_char;
    unsigned char rx_buff[10];
    char tx_buff[10]={0xff,0xf3};

    if (argc != 2) {
	fprintf(stderr, "%s: Use \"%s <dev>\"\n", argv[0], argv[0]);
	exit(1);
    }
    if((fd=open(argv[1], OPEN_FLAGS))  == -1){
	fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1], strerror(errno));
	exit(1);
    }
    if(configure(fd, 0)){
	printf("configure error\n");
	close(fd);
	return(-1);
    }

    /* since it doesn't work at first trial, re-run it */
    close(fd);
    sleep(1);
    if((fd=open(argv[1], OPEN_FLAGS))  == -1){
	fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1], strerror(errno));
	exit(1);
    }
    if(configure(fd, 1)){
	printf("configure error\n");
	close(fd);
	return(-1);
    }

    /* Ok, now initialize */
    tx_buff[0] = 0xff;
    if(write(fd,tx_buff,1) !=1){
	printf("write error\n");
	close(fd);
	return(-1);
    }
    sleep(1);
    tx_buff[0] = 0xf3;
    if(write(fd,tx_buff,1) !=1){
	printf("write error\n");
	close(fd);
	return(-1);
    }
    while(1) {
	n_char = read(fd, rx_buff, 10);
	printf("%i:",n_char);
	for(i=0; i < n_char; i++)
	    printf(" 0x%02x",rx_buff[i]);
	printf("\n");
    }
    return 0;
}





