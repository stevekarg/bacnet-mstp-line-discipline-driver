
/*
 * Dependency stuff for 2.2/2.4
 */
#ifndef _KIRK_SYSDEP_H_
#define _KIRK_SYSDEP_H_

#ifndef VERSION_CODE
#  define VERSION_CODE(vers,rel,seq) ( ((vers)<<16) | ((rel)<<8) | (seq) )
#endif

#if LINUX_VERSION_CODE < VERSION_CODE(2,2,0) /* not < 2.2 */
#  error "This kernel is too old: not supported by this file"
#endif
#if LINUX_VERSION_CODE > VERSION_CODE(2,5,0) /* not > 2.4, by now */
#  error "This kernel is too recent: not supported by this file"
#endif
#if (LINUX_VERSION_CODE & 0xff00) == 0x300 /* not 2.3 */
#  error "Please don't use linux-2.3, use 2.4 instead"
#endif

/* remember about the current version */
#if LINUX_VERSION_CODE < VERSION_CODE(2,3,0)
#  define LINUX_22
#else
#  define LINUX_24
#endif

/* bottom halves have changed */
#ifdef LINUX_22
#  define sysdep_enable_tasklet()     mark_bh(KEYBOARD_BH)
#else
#  define sysdep_enable_tasklet()     tasklet_schedule(&keyboard_tasklet)
#endif

#if LINUX_VERSION_CODE < VERSION_CODE(2,2,18)
#  define DECLARE_WAIT_QUEUE_HEAD(head) struct wait_queue *head = NULL
typedef  struct wait_queue *wait_queue_head_t;
#  define init_waitqueue_head(head) (*(head)) = NULL
#endif

/* kill_fasync had less arguments, and a different indirection in the first */
#ifndef LINUX_24
#  define kill_fasync(ptrptr,sig,band)  kill_fasync(*(ptrptr),(sig))
#endif

#endif /* _KIRK_SYSDEP_H_ */


