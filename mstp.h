/*
 * mstp.h common kernel-space/user-space definitions
 *
 * ####COPYRIGHTBEGIN####
 * -------------------------------------------
 * Copyright (c) 2004 Coleman Brumley (cbrumley@users.sourceforge.net)
 *
 * Based, in part, on code by:
 * Copyright (c) 2001 Alessandro Rubini (rubini@linux.it)
 * Copyright (c) 2001 CiaoLab S.r.L. (info@ciaolab.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to
 * The Free Software Foundation, Inc.
 * 59 Temple Place - Suite 330
 * Boston, MA  02111-1307, USA.
 *
 * (See the included file COPYING)
 *
 * -------------------------------------------
 * ####COPYRIGHTEND####
 */

#ifndef __MSTP_H__
#define __MSTP_H__

#ifdef __KERNEL__
#include "sysdep.h"
#endif

#include "mstp_ioctl.h"

/* First of all, my simple support for debug printouts */

#undef PDEBUG             /* undef it, just in case */
#ifdef MSTP_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "mstp: " fmt, ## args)
#    define DEBUG_CODE(code) code
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#  define DEBUG_CODE(code)
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define MSTP_MSG "mstp: "

#define MSTP_PACKET_LEN    8 /* no way out, unless you change the code */

extern unsigned char mstp_magic;

/* Timer defined, based on HZ = 1000 */
#define MSTP_1SEC   (HZ)     /* 1000 ms */
#define MSTP_500MS  (HZ/2)   /* 500 ms */
#define MSTP_100MS  (HZ/10)  /* 100 ms */
#define MSTP_10MS   (HZ/100) /* 10 ms */
#define MSTP_5MS    (HZ/50)  /* 5 ms */

/* Boolean Defines */
#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif



/* These are the baud rate hash definitions */
#define USERBAUD9600 '9'+'6'
#define USERBAUD1920 '1'+'9'
#define USERBAUD3840 '3'+'8'
#define USERBAUD5760 '5'+'7'
#define USERBAUD7680 '7'+'6'
#define USERBAUD1152 '1'+'1'

typedef unsigned char BOOLEAN;
typedef unsigned short UINT16;
typedef unsigned char UINT8;

// MS/TP Frame Type
// Frame Types 8 through 127 are reserved by ASHRAE.
#define FRAME_TYPE_TOKEN 0
#define FRAME_TYPE_POLL_FOR_MASTER 1
#define FRAME_TYPE_REPLY_TO_POLL_FOR_MASTER 2
#define FRAME_TYPE_TEST_REQUEST 3
#define FRAME_TYPE_TEST_RESPONSE 4
#define FRAME_TYPE_BACNET_DATA_EXPECTING_REPLY 5
#define FRAME_TYPE_BACNET_DATA_NOT_EXPECTING_REPLY 6
#define FRAME_TYPE_REPLY_POSTPONED 7

// The number of elements in the array InputBuffer[].
#define INPUT_BUFFER_SIZE (501)
#define MSTP_BROADCAST_ADDRESS 0xFF

// MS/TP Frame Format
// All frames are of the following format:
//
// Preamble: two octet preamble: X`55', X`FF'
// Frame Type: one octet
// Destination Address: one octet address
// Source Address: one octet address
// Length: two octets, most significant octet first, of the Data field
// Header CRC: one octet
// Data: (present only if Length is non-zero)
// Data CRC: (present only if Length is non-zero) two octets,
//           least significant octet first
// (pad): (optional) at most one octet of padding: X'FF'

// Used to accumulate the CRC on the data field of a frame.
static UINT16 DataCRC;

// Used to store the data length of a received frame.
static unsigned DataLength;

// Used to store the destination address of a received frame.
static UINT8 DestinationAddress;

// Used to count the number of received octets or errors.
// This is used in the detection of link activity.
static unsigned EventCount;

// Used to store the frame type of a received frame.
static UINT8 FrameType;

// The number of frames sent by this node during a single token hold.
// When this counter reaches the value Nmax_info_frames, the node must
// pass the token.
static unsigned FrameCount;

// Used to accumulate the CRC on the header of a frame.
static UINT8 HeaderCRC;

// Used as an index by the Receive State Machine, up to a maximum value of
// InputBufferSize.
static unsigned Index;

// An array of octets, used to store octets as they are received.
// InputBuffer is indexed from 0 to InputBufferSize-1.
// The maximum size of a frame is 501 octets.
// A smaller value for InputBufferSize may be used by some implementations.
static UINT8 InputBuffer[INPUT_BUFFER_SIZE];
static UINT8 OutputBuffer[INPUT_BUFFER_SIZE];
static int OutputBufferSize;

// "Next Station," the MAC address of the node to which This Station passes
// the token. If the Next_Station is unknown, Next_Station shall be equal to
// This_Station.
static UINT8 Next_Station;

// "Poll Station," the MAC address of the node to which This Station last
// sent a Poll For Master. This is used during token maintenance.
static UINT8 Poll_Station;

// A Boolean flag set to TRUE by the Receive State Machine if an error is
// detected during the reception of a frame. Set to FALSE by the main
// state machine.
static BOOLEAN ReceivedInvalidFrame;

// A Boolean flag set to TRUE by the Receive State Machine if a valid frame
// is received. Set to FALSE by the main state machine.
static BOOLEAN ReceivedValidFrame;

// A counter of transmission retries used for Token and Poll For Master
// transmission.
static unsigned RetryCount;

// A timer with nominal 5 millisecond resolution used to measure and
// generate silence on the medium between octets. It is incremented by a
// timer process and is cleared by the Receive State Machine when activity
// is detected and by the SendFrame procedure as each octet is transmitted.
// Since the timer resolution is limited and the timer is not necessarily
// synchronized to other machine events, a timer value of N will actually
// denote intervals between N-1 and N
static unsigned SilenceTimer;

// A timer used to measure and generate Reply Postponed frames.  It is
// incremented by a timer process and is cleared by the Master Node State
// Machine when a Data Expecting Reply Answer activity is completed.
static unsigned ReplyPostponedTimer;

// A Boolean flag set to TRUE by the master machine if this node is the
// only known master node.
static BOOLEAN SoleMaster;

// Used to store the Source Address of a received frame.
static UINT8 SourceAddress;

// The number of tokens received by this node. When this counter reaches the
// value Npoll, the node polls the address range between TS and NS for
// additional master nodes. TokenCount is set to zero at the end of the
// polling process.
static unsigned TokenCount;

// "This Station," the MAC address of this node. TS is generally read from a
// hardware DIP switch, or from nonvolatile memory. Valid values for TS are
// 0 to 254. The value 255 is used to denote broadcast when used as a
// destination address but is not allowed as a value for TS.
static UINT8 This_Station;

// This parameter represents the value of the Max_Info_Frames property of
// the node's Device object. The value of Max_Info_Frames specifies the
// maximum number of information frames the node may send before it must
// pass the token. Max_Info_Frames may have different values on different
// nodes. This may be used to allocate more or less of the available link
// bandwidth to particular nodes. If Max_Info_Frames is not writable in a
// node, its value shall be 1.
static unsigned Nmax_info_frames = 1;

// This parameter represents the value of the Max_Master property of the
// node's Device object. The value of Max_Master specifies the highest
// allowable address for master nodes. The value of Max_Master shall be
// less than or equal to 127. If Max_Master is not writable in a node,
// its value shall be 127.
static unsigned Nmax_master = 127;

// The number of tokens received or used before a Poll For Master cycle
// is executed: 50.
static unsigned Npoll = 50;

// The number of retries on sending Token: 1.
static unsigned Nretry_token = 1;

// The minimum number of DataAvailable or ReceiveError events that must be
// seen by a receiving node in order to declare the line "active": 4.
static unsigned Nmin_octets = 4;

// The minimum time without a DataAvailable or ReceiveError event within
// a frame before a receiving node may discard the frame: 60 bit times.
// (Implementations may use larger values for this timeout,
// not to exceed 100 milliseconds.)
// At 9600 baud, 60 bit times would be about 6.25 milliseconds
//FIXME -- This needs to be associated with baud rate instead of hardcoded 9600...
static unsigned Tframe_abort = 1 + ((1000 * 60) / 9600);

// The maximum idle time a sending node may allow to elapse between octets
// of a frame the node is transmitting: 20 bit times.
//static unsigned Tframe_gap = 20;

// The time without a DataAvailable or ReceiveError event before declaration
// of loss of token: 500 milliseconds.
static unsigned Tno_token = 500;

// The maximum time after the end of the stop bit of the final
// octet of a transmitted frame before a node must disable its
// EIA-485 driver: 15 bit times.
//static unsigned Tpostdrive = 15;
static unsigned Tpostdrive = 1 + ((1000 * 15) / 9600);

// The maximum time a node may wait after reception of a frame that expects
// a reply before sending the first octet of a reply or Reply Postponed
// frame: 250 milliseconds.
static unsigned Treply_delay = 250;

// The minimum time without a DataAvailable or ReceiveError event
// that a node must wait for a station to begin replying to a
// confirmed request: 255 milliseconds. (Implementations may use
// larger values for this timeout, not to exceed 300 milliseconds.)
static unsigned Treply_timeout = 255;

// Repeater turnoff delay. The duration of a continuous logical one state
// at the active input port of an MS/TP repeater after which the repeater
// will enter the IDLE state: 29 bit times < Troff < 40 bit times.
//static unsigned Troff = 30;

// The width of the time slot within which a node may generate a token:
// 10 milliseconds.
static unsigned Tslot = 10;

// The minimum time after the end of the stop bit of the final octet of a
// received frame before a node may enable its EIA-485 driver: 40 bit times.
// At 9600 baud, 40 bit times would be about 4.166 milliseconds
static unsigned Tturnaround = 1 + ((1000 * 40) / 9600);

// The maximum time a node may wait after reception of the token or
// a Poll For Master frame before sending the first octet of a frame:
// 15 milliseconds.
//static unsigned Tusage_delay = 15;

// The minimum time without a DataAvailable or ReceiveError event that a
// node must wait for a remote node to begin using a token or replying to
// a Poll For Master frame: 20 milliseconds. (Implementations may use
// larger values for this timeout, not to exceed 100 milliseconds.)
static unsigned Tusage_timeout = 20;

// These frames are available to vendors as proprietary (non-BACnet) frames.
// The first two octets of the Data field shall specify the unique vendor
// identification code, most significant octet first, for the type of
// vendor-proprietary frame to be conveyed. The length of the data portion
// of a Proprietary frame shall be in the range of 2 to 501 octets.
//
//FIXME (CLB):  How do we determine which proprietary frames we support?
//
#define FRAME_TYPE_PROPRIETARY_MIN 128
#define FRAME_TYPE_PROPRIETARY_MAX 255


static BOOLEAN ReceiveError; // TRUE when error detected during Rx octet
static BOOLEAN DataAvailable; // There is data in the buffer
//static UINT8 DataRegister; // stores the latest data


// receive FSM states
typedef enum
{
    MSTP_RECEIVE_STATE_IDLE,
    MSTP_RECEIVE_STATE_PREAMBLE,
    MSTP_RECEIVE_STATE_HEADER,
    MSTP_RECEIVE_STATE_HEADER_CRC,
    MSTP_RECEIVE_STATE_DATA,
    MSTP_RECEIVE_STATE_DATA_CRC,
} MSTP_RECEIVE_STATE;

// master node FSM states
typedef enum
{
    MSTP_MASTER_STATE_INITIALIZE,
    MSTP_MASTER_STATE_IDLE,
    MSTP_MASTER_STATE_USE_TOKEN,
    MSTP_MASTER_STATE_WAIT_FOR_REPLY,
    MSTP_MASTER_STATE_DONE_WITH_TOKEN,
    MSTP_MASTER_STATE_PASS_TOKEN,
    MSTP_MASTER_STATE_NO_TOKEN,
    MSTP_MASTER_STATE_POLL_FOR_MASTER,
    MSTP_MASTER_STATE_ANSWER_DATA_REQUEST,
} MSTP_MASTER_STATE;

static char* rfsm_strings[] = {"Idle","Preamble","Header","HeaderCRC","Data","DataCRC"};
static char* mnsm_strings[] = {"Initialize","Idle","Use Token","Wait for Reply","Done with Token","Pass Token","No Token","Poll For Master","Answer Data Request"};

//Initial states
static MSTP_RECEIVE_STATE rfsm_state = MSTP_RECEIVE_STATE_IDLE;

// When a master node is powered up or reset,
// it shall unconditionally enter the INITIALIZE state.
static MSTP_MASTER_STATE state = MSTP_MASTER_STATE_INITIALIZE;

#endif /* __MSTP_H__ */
