#!/bin/sh

# Simple wrapper to insmod

module="kirk"
device="kirkmouse"
mode="664"

# Group: since distributions do it differently, look for wheel or use staff
if grep '^staff:' /etc/group > /dev/null; then
    group="staff"
else
    group="wheel"
fi

# invoke insmod with all arguments we got
# and use a pathname, as newer modutils don't look in . by default
/sbin/insmod -f ./$module.o $* || exit 1

minor=`cat /proc/misc | awk "\\$2==\"$module\" {print \\$1}"`
mknod /dev/$device c 10 $minor

chgrp $group /dev/$device
chmod $mode /dev/$device

exit 0
