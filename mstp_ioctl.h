#ifndef __MSTP_IOCTL_H__
#define __MSTP_IOCTL_H__

#include <linux/ioctl.h>

/* ioctl defines
 *
 * From /usr/src/linux/Documentation/ioctl-number.txt:
 * 0xB2 isn't used, so we'll grab it for our driver
 * I'm not sure how to register this range for real, but this should do
 *
 * Code    Seq#    Include File            Comments
 * =========================================================================
 * 0xBA    C0-FF   mstp.h                  cbrumley at users.sourceforge.net
 */

#define MSTP_IOC_MAGIC 0xBA

#define MSTP_IOC_SETMAXMASTER _IOW(MSTP_IOC_MAGIC,0xC0,unsigned)
#define MSTP_IOC_SETMAXINFOFRAMES _IOW(MSTP_IOC_MAGIC,0xC1,unsigned)
#define MSTP_IOC_SETMACADDRESS _IOW(MSTP_IOC_MAGIC,0xC2,unsigned char)
#define MSTP_IOC_GETMAXMASTER _IOR(MSTP_IOC_MAGIC,0xC3,unsigned)
#define MSTP_IOC_GETMAXINFOFRAMES _IOR(MSTP_IOC_MAGIC,0xC4,unsigned)
#define MSTP_IOC_GETMACADDRESS _IOR(MSTP_IOC_MAGIC,0xC5,unsigned char)

#define MSTP_MIN_NR 0xC0
#define MSTP_MAX_NR 0xC5

#endif

