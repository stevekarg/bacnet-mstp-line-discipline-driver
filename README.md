# BACnet MSTP Line Discipline Driver

BACnet MSTP Line Discipline Driver
originally written by Coleman and Steve

Based on the Kernel Infra-Red Keyboard driver.

It implements a line discipline to encode and decode serial data as packets.

Quick start:
	- compile with "make"
	- the "mstpdump" program can be used to see hex bytes
	- insmod with "insmod mstp.o"
	- run with "./mstprun /dev/ttyS0"
	- when "mstprun" runs, the serial port can't be used for other purposes
