/*
 * mstp.c - main source file for handling mstp packets
 *
 * ####COPYRIGHTBEGIN####
 * -------------------------------------------
 * Copyright (c) 2004 Coleman Brumley (cbrumley at users.sourceforge.net)
 * Copyright (c) 2003 Steve Karg      (skarg at users.sourceforge.net)
 *
 * Based, in part, on code by:
 * kirk.c --> a device driver for IR keyboards
 *     Copyright (c) 2001 Alessandro Rubini (rubini at linux.it)
 *     Copyright (c) 2001 CiaoLab S.r.L. (info at ciaolab.com)
 *
 * slip.c --> SLIP protocol implementation
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this program; if not, write to 
 * The Free Software Foundation, Inc.
 * 59 Temple Place - Suite 330 
 * Boston, MA  02111-1307, USA.
 * 
 * (See the included file COPYING)
 * 
 * -------------------------------------------
 * ####COPYRIGHTEND####
*/

#ifndef MODULE
#  define MODULE
#endif

#ifndef __KERNEL__
#  define __KERNEL__
#endif


#define EXPORT_SYMTAB
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/timer.h>
#include <linux/tty_ldisc.h>
#include <linux/config.h>
#include <linux/version.h>
#include <linux/serial_reg.h>
#include <linux/serial.h>
#include <linux/serialP.h>

#define DEV_MSTP 345 /* just random */

#include "mstp.h"

#define MODULE_VERSION "1.0"
#define MODULE_NAME "BACnet MS/TP Kernel Module"

struct timer_list mstp_timer;

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,2,18)
#  warning "Can't support BACnet MS/TP, please upgrade to 2.2.18 or later"
#  define MSTP_NO_LEDS
#endif

#define HW_LEN 8

/* /proc data structure */
struct mstp_data_t {
      char value[HW_LEN + 1];
};

struct mstp_data_t mstp_data;

/* /proc entry */
static struct proc_dir_entry *mstp_file;

/* module state */
unsigned char mod_state;
#define STATE_Ready  'R'
#define STATE_Done   'D'

static void Master_Node_FSM(void);
static void SendFrame( UINT8 SendFrameType,UINT8 destination,UINT8 *data,unsigned data_len);

struct tty_struct *mstp_tty = NULL;

struct timeval tv1, tv2; // do_gettimeofday fills these for our postdrive timeout

/*
 * The buffer is global
 */

static unsigned char *mstp_head, *mstp_tail;

static BOOLEAN tx_empty()
{
    //unsigned char arg = 0;
    unsigned char lsr = 0;
    mm_segment_t fs;
    if(!mstp_tty) return -1;

    /*
    *  The ioctl() function, or actually set_modem_info() in serial.c
    *  expects a pointer to the argument in user space. To hack us
    *  around this, we use the set_fs() function to fool the routines 
    *  that check if they are called from user space. We also need 
    *  to send a pointer to the argument so get_user() gets happy. 
    *
    *  On x86 platforms Linux uses FS register to keep the data segment
    *  register with which  access user segment register. If a sys_call is made
    *  and the kernel needs to copy data from user space then FS will have the
    *  value USER_DS. Many kernel functions copy or return data from/to user
    *  space.
    *
    *  I don't think this is fast enough, so you'll note in SendFrame,
    *  we call inb directly on the LSR register to check the TEMT bit
    */

    fs = get_fs();
    set_fs(get_ds());
    lsr = mstp_tty->driver.ioctl(mstp_tty, NULL, TIOCSERGETLSR,(unsigned long)NULL);
    set_fs(fs);
    return ((lsr & UART_LSR_TEMT) ? TRUE : FALSE);
}

static unsigned int tx_off()
{
    unsigned long mcr = 0;
    //unsigned long status = 0;
    mm_segment_t fs;
    //unsigned long flags;
    if(!mstp_tty) return -1;

    /*
    *  The ioctl() function, or actually set_modem_info() in serial.c
    *  expects a pointer to the argument in user space. To hack us
    *  around this, we use the set_fs() function to fool the routines 
    *  that check if they are called from user space. We also need 
    *  to send a pointer to the argument so get_user() gets happy. 
    *
    *  On x86 platforms Linux uses FS register to keep the data segment
    *  register with which  access user segment register. If a sys_call is made
    *  and the kernel needs to copy data from user space then FS will have the
    *  value USER_DS. Many kernel functions copy or return data from/to user
    *  space.
    */

    //write 0 to MCR register, RTS bit to disable the transmitter

    mcr = UART_MCR_DTR;  //Loop = 0, OUT2 = 0, RTS = 0, DTR = 1

    fs = get_fs();
    set_fs(get_ds());
    mstp_tty->driver.ioctl(mstp_tty, NULL, TIOCMSET, mcr);
    set_fs(fs);
    return 0;
}

static unsigned int tx_on()
{
    unsigned long mcr = 0;
    //unsigned long status = 0;
    mm_segment_t fs;
    //unsigned long flags;
    if(!mstp_tty) return -1;

    /*
    *  The ioctl() function, or actually set_modem_info() in serial.c
    *  expects a pointer to the argument in user space. To hack us
    *  around this, we use the set_fs() function to fool the routines 
    *  that check if they are called from user space. We also need 
    *  to send a pointer to the argument so get_user() gets happy. 
    *
    *  On x86 platforms Linux uses FS register to keep the data segment
    *  register with which  access user segment register. If a sys_call is made
    *  and the kernel needs to copy data from user space then FS will have the
    *  value USER_DS. Many kernel functions copy or return data from/to user
    *  space.
    */

    mcr = UART_MCR_RTS | UART_MCR_DTR; //Loop = 0, OUT2 = 0, RTS = 1, DTR = 1

    fs = get_fs();
    set_fs(get_ds());
    mstp_tty->driver.ioctl(mstp_tty, NULL, TIOCMSET,  mcr); 
    set_fs(fs);
    return 0;
}

/*
 * Timer function, in charge of SilenceTimer
 */

static void mstp_10ms_timer_function()
{
    SilenceTimer += 10; 		/* increment SilenceTimer by 10ms */
    /* and re-register yourself */
    if(mod_state != STATE_Done)
    {
	mstp_timer.expires = jiffies + MSTP_5MS; /* 10 ms when HZ is 1000, 5 ms if HZ is 500*/
	add_timer(&mstp_timer);
	Master_Node_FSM();
    }
}  

// Accumulate "dataValue" into the CRC in crcValue.
// Return value is updated CRC
//
//  Assumes that "unsigned char" is equivalent to one octet.
//  Assumes that "unsigned int" is 16 bits.
//  The ^ operator means exclusive OR.
// Note: This function is copied directly from the BACnet standard.
UINT8 CalcHeaderCRC(UINT8 dataValue, UINT8 crcValue)
{
    unsigned int crc;

    crc = crcValue ^ dataValue;   /* XOR C7..C0 with D7..D0 */

    /* Exclusive OR the terms in the table (top down) */
    crc = crc ^ (crc << 1) ^ (crc << 2) ^ (crc << 3) ^ (crc << 4) ^ (crc << 5) ^ (crc << 6) ^ (crc << 7);

    /* Combine bits shifted out left hand end */
    return (crc & 0xfe) ^ ((crc >> 8) & 1);
}

// Accumulate "dataValue" into the CRC in crcValue.
//  Return value is updated CRC
//
//  Assumes that "unsigned char" is equivalent to one octet.
//  Assumes that "unsigned int" is 16 bits.
//  The ^ operator means exclusive OR.
// Note: This function is copied directly from the BACnet standard.
UINT16 CalcDataCRC(UINT8 dataValue, UINT16 crcValue)
{
    unsigned int crcLow;

    crcLow = (crcValue & 0xff) ^ dataValue;        /* XOR C7..C0 with D7..D0 */

    /* Exclusive OR the terms in the table (top down) */
    return (crcValue >>8) ^ (crcLow << 8)  ^ (crcLow <<3) ^ (crcLow <<12)  ^ (crcLow >> 4) ^ (crcLow & 0x0f) ^ ((crcLow & 0x0f) << 7);
}

void Receive_Frame_FSM(UINT8 DataRegister)
{
    //printk(MSTP_MSG "RFSM State:       %s %02X\n", rfsm_strings[state],DataRegister);
    switch (rfsm_state)
    {
    // In the IDLE state, the node waits for the beginning of a frame.
    case MSTP_RECEIVE_STATE_IDLE:
	// EatAnError
	if (ReceiveError == TRUE)
	{
	    ReceiveError = FALSE;
	    SilenceTimer = 0;
	    EventCount++;
	    // wait for the start of a frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}
	else
	{
	    //only get here if there's data, so it always true from the ISR
	    //if(count > 0)
	//	DataAvailable = TRUE;
	  //  else
	//	DataAvailable = FALSE;
	    if (DataAvailable == TRUE)
	    {
		// Preamble1
		if (DataRegister == 0x55)
		{
		    DataAvailable = FALSE;
		    SilenceTimer = 0;
		    EventCount++;
		    // receive the remainder of the frame.
		    rfsm_state = MSTP_RECEIVE_STATE_PREAMBLE;
		}
		 //EatAnOctet
		else
		{
		    DataAvailable = FALSE;
		    SilenceTimer = 0;
		    EventCount++;
		    // wait for the start of a frame.
		    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
		}
	    }
	}
    break;
    case MSTP_RECEIVE_STATE_PREAMBLE:
	// Timeout
	if (SilenceTimer > Tframe_abort)
	{
	    // a correct preamble has not been received
	    // wait for the start of a frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}

	// Error
	if (ReceiveError == TRUE)
	{
	    ReceiveError = FALSE;
	    SilenceTimer = 0;
	    EventCount++;
	    // wait for the start of a frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}
	else
	{
	    if (DataAvailable == TRUE)
	    {
		// Preamble2
		if (DataRegister ==0xFF)
		{
		    DataAvailable = FALSE;
		    SilenceTimer = 0;
		    EventCount++;
		    Index = 0;
		    HeaderCRC = 0xFF;
		    // receive the remainder of the frame.
		    rfsm_state = MSTP_RECEIVE_STATE_HEADER;
		}
		// RepeatedPreamble1
		else if (DataRegister == 0x55)
		{
		    DataAvailable = FALSE;
		    SilenceTimer = 0;
		    EventCount++;
		    // wait for the second preamble octet.
		    rfsm_state = MSTP_RECEIVE_STATE_PREAMBLE;
		}
		// NotPreamble
		else
		{
		    DataAvailable = FALSE;
		    SilenceTimer = 0;
		    EventCount++;
		    // wait for the start of a frame.
		    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
		}
	    }
	}
    break;
    case MSTP_RECEIVE_STATE_HEADER:
	// Timeout
	if (SilenceTimer > Tframe_abort)
	{
	    // indicate that an error has occurred during the reception of a frame
	    ReceivedInvalidFrame = TRUE;
	    // wait for the start of a frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}

	// Error
	if (ReceiveError == TRUE)
	{
	    ReceiveError = FALSE;
	    SilenceTimer = 0;
	    EventCount++;
	    // indicate that an error has occurred during the reception of a frame
	    ReceivedInvalidFrame = TRUE;
	    // wait for the start of a frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}
	else if (DataAvailable == TRUE)
	{
	    // FrameType
	    if (Index == 0)
	    {
		SilenceTimer = 0;
		EventCount++;
		HeaderCRC = CalcHeaderCRC(DataRegister,HeaderCRC);
		FrameType = DataRegister;
		DataAvailable = FALSE;
		Index = 1;
		rfsm_state = MSTP_RECEIVE_STATE_HEADER;
	    }
	    // Destination
	    else if (Index == 1)
	    {
		SilenceTimer = 0;
		EventCount++;
		HeaderCRC = CalcHeaderCRC(DataRegister,HeaderCRC);
		DestinationAddress = DataRegister;
		DataAvailable = FALSE;
		Index = 2;
		rfsm_state = MSTP_RECEIVE_STATE_HEADER;
	    }
	    // Source
	    else if (Index == 2)
	    {
		SilenceTimer = 0;
		EventCount++;
		HeaderCRC = CalcHeaderCRC(DataRegister,HeaderCRC);
		SourceAddress = DataRegister;
		DataAvailable = FALSE;
		Index = 3;
		rfsm_state = MSTP_RECEIVE_STATE_HEADER;
	    }
	    // Length1
	    else if (Index == 3)
	    {
		SilenceTimer = 0;
		EventCount++;
		HeaderCRC = CalcHeaderCRC(DataRegister,HeaderCRC);
		DataLength = DataRegister * 256;
		DataAvailable = FALSE;
		Index = 4;
		rfsm_state = MSTP_RECEIVE_STATE_HEADER;
	    }
	    // Length2
	    else if (Index == 4)
	    {
		SilenceTimer = 0;
		EventCount++;
		HeaderCRC = CalcHeaderCRC(DataRegister,HeaderCRC);
		DataLength += DataRegister;
		DataAvailable = FALSE;
		Index = 5;
		rfsm_state = MSTP_RECEIVE_STATE_HEADER;
	    }
	    // HeaderCRC
	    else if (Index == 5)
	    {
		SilenceTimer = 0;
		EventCount++;
		HeaderCRC = CalcHeaderCRC(DataRegister,HeaderCRC);
		DataAvailable = FALSE;
		rfsm_state = MSTP_RECEIVE_STATE_HEADER_CRC;
	    }
	    // not per MS/TP standard, but it is a case not covered
	    else
	    {
		ReceiveError = FALSE;
		SilenceTimer = 0;
		EventCount++;
		// indicate that an error has occurred during the reception of a frame
		ReceivedInvalidFrame = TRUE;
		// wait for the start of a frame.
		rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	    }
	}
    break;
    // In the HEADER_CRC state, the node validates the CRC on the fixed
    // message header.
    case MSTP_RECEIVE_STATE_HEADER_CRC:
	// BadCRC
	if (HeaderCRC != 0x55)
	{
	    // indicate that an error has occurred during the reception of a frame
	    ReceivedInvalidFrame = TRUE;
	    // wait for the start of the next frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}
	else
	{
	    if (((DestinationAddress == This_Station) ||
	    (DestinationAddress == MSTP_BROADCAST_ADDRESS))
	    && (SourceAddress != This_Station)) //insure we aren't talking to ourselves, not part of the spec
	    {
		// FrameTooLong
		if (DataLength > INPUT_BUFFER_SIZE)
		{
		    // indicate that a frame with an illegal or unacceptable data length
		    // has been received
		    ReceivedInvalidFrame = TRUE;
		    // wait for the start of the next frame.
		    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
		}
		// NoData
		else if (DataLength == 0)
		{
		    // indicate that a frame with no data has been received
		    ReceivedValidFrame = TRUE;
		    // wait for the start of the next frame.
		    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
		}
		// Data
		else
		{
		    Index = 0;
		    DataCRC = 0xFFFF;
		    // receive the data portion of the frame.
		    rfsm_state = MSTP_RECEIVE_STATE_DATA;
		}
	    }
	    // NotForUs
	    else
	    {
		// wait for the start of the next frame.
		rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	    }
	}
    break;
    // In the DATA state, the node waits for the data portion of a frame.
    case MSTP_RECEIVE_STATE_DATA:
	// Timeout
	if (SilenceTimer > Tframe_abort)
	{
	    // indicate that an error has occurred during the reception of a frame
	    ReceivedInvalidFrame = TRUE;
	    // wait for the start of the next frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}
	// Error
	if (ReceiveError == TRUE)
	{
	    ReceiveError = FALSE;
	    SilenceTimer = 0;
	    // indicate that an error has occurred during the reception of a frame
	    ReceivedInvalidFrame = TRUE;
	    // wait for the start of the next frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}
	else if (DataAvailable == TRUE)
	{
	    //CLB this is where we build our socket packet for the upper layers
	    //this is based on frametype
	    // DataOctet
	    if (Index < DataLength)
	    {
		SilenceTimer = 0;
		DataCRC = CalcDataCRC(DataRegister,DataCRC);
		InputBuffer[Index] = DataRegister;
		DataAvailable = FALSE;
		Index++;
		//CLB This is where we send to the upper layers
		rfsm_state = MSTP_RECEIVE_STATE_DATA;
	    }
	    // CRC1
	    if (Index == DataLength)
	    {
		SilenceTimer = 0;
		DataCRC = CalcDataCRC(DataRegister,DataCRC);
		DataAvailable = FALSE;
		Index++; // Index now becomes the number of data octets
		rfsm_state = MSTP_RECEIVE_STATE_DATA;
	    }
	    // CRC2
	    if (Index == (DataLength + 1))
	    {
		SilenceTimer = 0;
		DataCRC = CalcDataCRC(DataRegister,DataCRC);
		DataAvailable = FALSE;
		rfsm_state = MSTP_RECEIVE_STATE_DATA_CRC;
	    }
	}
    break;
    // In the DATA_CRC state, the node validates the CRC of the message data.
    case MSTP_RECEIVE_STATE_DATA_CRC:
	// GoodCRC
	if (DataCRC == 0xF0B8)
	{
	    // indicate the complete reception of a valid frame
	    ReceivedValidFrame = TRUE;

	    // now might be a good time to process the message or
	    // copy the data to a buffer so that we can process the message

	    // wait for the start of the next frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}
	// BadCRC
	else
	{
	    // to indicate that an error has occurred during the reception of a frame
	    ReceivedInvalidFrame = TRUE;
	    // wait for the start of the next frame.
	    rfsm_state = MSTP_RECEIVE_STATE_IDLE;
	}
    break;
    default:
	// shouldn't get here - but if we do...
	rfsm_state = MSTP_RECEIVE_STATE_IDLE;
    break;
    }

return;
}

static void Master_Node_FSM(void)
{
  switch (state)
  {
    case MSTP_MASTER_STATE_INITIALIZE:
      // DoneInitializing
      // This_Station = 0; 
      // indicate that the next station is unknown
      Next_Station = This_Station;
      Poll_Station = This_Station;
      // cause a Poll For Master to be sent when this node first
      // receives the token
      TokenCount = Npoll;
      SoleMaster = FALSE;
      ReceivedValidFrame = FALSE;
      ReceivedInvalidFrame = FALSE;
      OutputBufferSize	 = 0;
      SilenceTimer	 = 0;
      state = MSTP_MASTER_STATE_IDLE;
      break;
    // In the IDLE state, the node waits for a frame.
    case MSTP_MASTER_STATE_IDLE:
      // LostToken
      if (SilenceTimer >= Tno_token)
      {
        // assume that the token has been lost
        state = MSTP_MASTER_STATE_NO_TOKEN;
	//printk(MSTP_MSG "LostToken SilenceTimer = %d\n",SilenceTimer);
      }
      // ReceivedInvalidFrame
      else if (ReceivedInvalidFrame == TRUE)
      {
        // invalid frame was received
        ReceivedInvalidFrame = FALSE;
        // wait for the next frame
        state = MSTP_MASTER_STATE_IDLE;
	//printk(MSTP_MSG "ReceivedInvalidFrame\n");
      }
      // ReceivedUnwantedFrame
      else if (ReceivedValidFrame == TRUE)
      {
        if ((DestinationAddress != This_Station) ||
            (DestinationAddress != MSTP_BROADCAST_ADDRESS))
        {
          // an unexpected or unwanted frame was received.
          ReceivedValidFrame = FALSE;
          // wait for the next frame
          state = MSTP_MASTER_STATE_IDLE;
	  //printk(MSTP_MSG "ReceivedUnwantedFrame\n");
        }
	//ReceivedDataNoReply
        // DestinationAddress is equal to 255 (broadcast) and
        // FrameType has a value of Token, BACnet Data Expecting Reply, Test_Request,
        // or a proprietary type known to this node that expects a reply
        // (such frames may not be broadcast), or
        else if ((DestinationAddress == MSTP_BROADCAST_ADDRESS) &&
             ((FrameType == FRAME_TYPE_TOKEN) ||
              (FrameType == FRAME_TYPE_BACNET_DATA_EXPECTING_REPLY) ||
              (FrameType == FRAME_TYPE_TEST_REQUEST)))
        {
          // an unexpected or unwanted frame was received.
          ReceivedValidFrame = FALSE;
          // wait for the next frame
          state = MSTP_MASTER_STATE_IDLE;
	  //printk(MSTP_MSG "ReceivedDataNoReply\n");
        }
        // FrameType has a value that indicates a standard or proprietary type
        // that is not known to this node.
        // FIXME: change this if you add a proprietary type
        else if /*(*/(FrameType >= FRAME_TYPE_PROPRIETARY_MIN) /*&&*/
          /*(FrameType <= FRAME_TYPE_PROPRIETARY_MAX))*/
          /* unnecessary if FrameType is UINT8 with max of 255 */
        {
          // an unexpected or unwanted frame was received.
          ReceivedValidFrame = FALSE;
          // wait for the next frame
          state = MSTP_MASTER_STATE_IDLE;
	  //printk(MSTP_MSG "UnknownFrameType\n");
        }
        // ReceivedToken
        else if ((DestinationAddress == This_Station) &&
                 (FrameType == FRAME_TYPE_TOKEN))
        {
          ReceivedValidFrame = FALSE;
          FrameCount = 0;
          SoleMaster = FALSE;
          state = MSTP_MASTER_STATE_USE_TOKEN;
	  //printk(MSTP_MSG "ReceivedToken\n");
        }
          // ReceivedPFM
        else if ((DestinationAddress == This_Station) &&
                 (FrameType == FRAME_TYPE_POLL_FOR_MASTER))
        {
          SendFrame( FRAME_TYPE_REPLY_TO_POLL_FOR_MASTER, SourceAddress, NULL,0);
          ReceivedValidFrame = FALSE;
          // wait for the next frame
          state = MSTP_MASTER_STATE_IDLE;
	  //printk(MSTP_MSG "ReceivedPFM\n");
        }
        // ReceivedDataNoReply
        // or a proprietary type known to this node that does not expect a reply
        else if (((DestinationAddress == This_Station) ||
                  (DestinationAddress == MSTP_BROADCAST_ADDRESS)) &&
                 ((FrameType == FRAME_TYPE_BACNET_DATA_NOT_EXPECTING_REPLY) ||
                //  (FrameType == FRAME_TYPE_PROPRIETARY_0) ||
                  (FrameType == FRAME_TYPE_TEST_RESPONSE)))
        {
          // FIXME: indicate successful reception to the higher layers
          // i.e. Process this frame!
          ReceivedValidFrame = FALSE;
          // wait for the next frame
          state = MSTP_MASTER_STATE_IDLE;
	  //printk(MSTP_MSG "ReceivedDataNoReply\n");
        }
        // ReceivedDataNeedingReply
        // or a proprietary type known to this node that expects a reply
        else if ((DestinationAddress == This_Station) &&
                 ((FrameType == FRAME_TYPE_BACNET_DATA_EXPECTING_REPLY) ||
                //  (FrameType == FRAME_TYPE_PROPRIETARY) ||
                  (FrameType == FRAME_TYPE_TEST_REQUEST)))
        {
          ReplyPostponedTimer = 0;
          // indicate successful reception to the higher layers
          // (management entity in the case of Test_Request);
          ReceivedValidFrame = FALSE;
          state = MSTP_MASTER_STATE_ANSWER_DATA_REQUEST;
	  //printk(MSTP_MSG "ReceivedDataNeedingReply\n");
        }
      }
      break;
    // In the USE_TOKEN state, the node is allowed to send one or
    // more data frames. These may be BACnet Data frames or
    // proprietary frames.
    case MSTP_MASTER_STATE_USE_TOKEN:
      // NothingToSend
	    // FIXME: If there is no data frame awaiting transmission,
      {
        FrameCount = Nmax_info_frames;
        state = MSTP_MASTER_STATE_DONE_WITH_TOKEN;
	//printk(MSTP_MSG "NothingToSend\n");
      }
      // SendNoWait
      // FIXME: If there is a frame awaiting transmission that
      // is of type Test_Response, BACnet Data Not Expecting Reply,
      // or a proprietary type that does not expect a reply,
      // FIXME:  The intent here is to use a send queue
      // 	 here, we'd pop the head from the queue
      // 	 and send it
//      {
//        // transmit the data frame
//        SendFrame(?????????????);
//        FrameCount++;
//        state = MSTP_MASTER_STATE_DONE_WITH_TOKEN;
//      }
      // SendAndWait
      // FIXME:	If there is a frame awaiting transmission that is of
      // type Test_Request, BACnet Data Expecting Reply, or
      // a proprietary type that expects a reply,
//      {
//        // transmit the data frame
//        SendFrame();
//        FrameCount++;
//        state = MSTP_MASTER_STATE_WAIT_FOR_REPLY;
//      }
       break;
    // In the WAIT_FOR_REPLY state, the node waits for
    // a reply from another node.
    case MSTP_MASTER_STATE_WAIT_FOR_REPLY:
      // ReplyTimeout
      if (SilenceTimer >= Treply_timeout)
      {
        // assume that the request has failed
        FrameCount = Nmax_info_frames;
        state = MSTP_MASTER_STATE_DONE_WITH_TOKEN;
        // Any retry of the data frame shall await the next entry
        // to the USE_TOKEN state. (Because of the length of the timeout,
        // this transition will cause the token to be passed regardless
        // of the initial value of FrameCount.)
      }
      // InvalidFrame
      else if ((SilenceTimer < Treply_timeout) &&
        (ReceivedInvalidFrame == TRUE))
      {
        // error in frame reception
        ReceivedInvalidFrame = FALSE;
        state = MSTP_MASTER_STATE_DONE_WITH_TOKEN;
      }
      // ReceivedReply
      // or a proprietary type that indicates a reply
      else if ((SilenceTimer < Treply_timeout) &&
        (ReceivedValidFrame == TRUE) &&
        (DestinationAddress == This_Station) &&
        ((FrameType == FRAME_TYPE_TEST_RESPONSE) ||
         //(FrameType == FRAME_TYPE_PROPRIETARY_0) ||
         (FrameType == FRAME_TYPE_BACNET_DATA_NOT_EXPECTING_REPLY)))
      {
        // FIXME: indicate successful reception to the higher layers
        ReceivedValidFrame = FALSE;
        state = MSTP_MASTER_STATE_DONE_WITH_TOKEN;
      }
      // ReceivedPostpone
      else if ((SilenceTimer < Treply_timeout) &&
          (ReceivedValidFrame == TRUE) &&
          (DestinationAddress == This_Station) &&
          (FrameType == FRAME_TYPE_REPLY_POSTPONED))
      {
        // FIXME: then the reply to the message has been postponed until a later time.
        // So, what does this really mean?
        ReceivedValidFrame = FALSE;
        state = MSTP_MASTER_STATE_DONE_WITH_TOKEN;
      }
      // ReceivedUnexpectedFrame
      else if ((SilenceTimer < Treply_timeout) &&
          (ReceivedValidFrame == TRUE) &&
          (DestinationAddress != This_Station))
      //the expected reply should not be broadcast)
      {
        // an unexpected frame was received
        // This may indicate the presence of multiple tokens.
        ReceivedValidFrame = FALSE;
        // Synchronize with the network.
        // This action drops the token.
        state = MSTP_MASTER_STATE_IDLE;
      }
      // ReceivedUnexpectedFrame
      else if ((SilenceTimer < Treply_timeout) &&
        (ReceivedValidFrame == TRUE) &&
        ((FrameType == FRAME_TYPE_TEST_RESPONSE) ||
         //(FrameType == FRAME_TYPE_PROPRIETARY_0) ||
         (FrameType == FRAME_TYPE_BACNET_DATA_NOT_EXPECTING_REPLY)))
      {
        // An unexpected frame was received.
        // This may indicate the presence of multiple tokens.
        ReceivedValidFrame = FALSE;
        // Synchronize with the network.
        // This action drops the token.
        state = MSTP_MASTER_STATE_IDLE;
      }
      break;
    // The DONE_WITH_TOKEN state either sends another data frame,
    // passes the token, or initiates a Poll For Master cycle.
    case MSTP_MASTER_STATE_DONE_WITH_TOKEN:
      // SendAnotherFrame
      if (FrameCount < Nmax_info_frames)
      {
        // then this node may send another information frame
        // before passing the token.
        state = MSTP_MASTER_STATE_USE_TOKEN;
	//printk(MSTP_MSG "SendAnotherFrame\n");
      }
      // SoleMaster
      else if ((FrameCount >= Nmax_info_frames) &&
        (TokenCount < Npoll) &&
        (SoleMaster == TRUE))
      {
        // there are no other known master nodes to
        // which the token may be sent (true master-slave operation).
        FrameCount = 0;
        TokenCount++;
        state = MSTP_MASTER_STATE_USE_TOKEN;
	//printk(MSTP_MSG "SoleMaster\n");
      }
      // SendToken
      else if (((FrameCount >= Nmax_info_frames) &&
        (TokenCount < Npoll) &&
        (SoleMaster == FALSE)) ||
        // The comparison of NS and TS+1 eliminates the Poll For Master
        // if there are no addresses between TS and NS, since there is no
        // address at which a new master node may be found in that case.
        (Next_Station == (UINT8)((This_Station +1) % (Nmax_master + 1))))
      {
        TokenCount++;
        // transmit a Token frame to NS
        SendFrame( FRAME_TYPE_TOKEN, Next_Station, NULL,0);
        RetryCount = 0;
        EventCount = 0;
        state = MSTP_MASTER_STATE_PASS_TOKEN;
	//printk(MSTP_MSG "SendToken\n");
      }
      // SendMaintenancePFM
      else if ((FrameCount >= Nmax_info_frames) &&
        (TokenCount >= Npoll) &&
        ((UINT8)((Poll_Station + 1) % (Nmax_master + 1)) != Next_Station))
      {
	Poll_Station=(Poll_Station!=Nmax_master)?Poll_Station+1:0;
        //Poll_Station = (Poll_Station + 1) % (Nmax_master + 1);
	//if(Poll_Station != This_Station) //don't send it if it's going to us
	SendFrame( FRAME_TYPE_POLL_FOR_MASTER, Poll_Station, NULL,0);
        RetryCount = 0;
        state = MSTP_MASTER_STATE_POLL_FOR_MASTER;
	//printk(MSTP_MSG "SendMaintenancePFM\n");
      }
      // ResetMaintenancePFM
      else if ((FrameCount >= Nmax_info_frames) &&
        (TokenCount >= Npoll) &&
        ((UINT8)((Poll_Station + 1) % (Nmax_master + 1)) == Next_Station) &&
        (SoleMaster == FALSE))
      {
        Poll_Station = This_Station;
        // transmit a Token frame to NS
        SendFrame( FRAME_TYPE_TOKEN, Next_Station, NULL,0);
        RetryCount = 0;
        TokenCount = 0;
        EventCount = 0;
        state = MSTP_MASTER_STATE_PASS_TOKEN;
	//printk(MSTP_MSG "ResetMaintenancePFM\n");
      }
      // SoleMasterRestartMaintenancePFM
      else if ((FrameCount >= Nmax_info_frames) &&
        (TokenCount >= Npoll) &&
        ((UINT8)((Poll_Station + 1) % (Nmax_master + 1)) == Next_Station) &&
        (SoleMaster == TRUE))
      {
	Poll_Station=(Next_Station!=Nmax_master)?Poll_Station+1:0;
        //Poll_Station = (Next_Station +1) % (Nmax_master + 1);
	//if(Poll_Station != This_Station) //don't send it if it's going to us
	SendFrame( FRAME_TYPE_POLL_FOR_MASTER, Poll_Station, NULL,0);
        // no known successor node
        Next_Station = This_Station;
        RetryCount = 0;
        TokenCount = 0;
        EventCount = 0;
        // find a new successor to TS
        state = MSTP_MASTER_STATE_POLL_FOR_MASTER;
	//printk(MSTP_MSG "SoleMasterRestartMaintenancePFM\n");
      }
      break;
    // The PASS_TOKEN state listens for a successor to begin using
    // the token that this node has just attempted to pass.
    case MSTP_MASTER_STATE_PASS_TOKEN:
      // SawTokenUser
      if ((SilenceTimer < Tusage_timeout) &&
        (EventCount > Nmin_octets))
      {
        // Assume that a frame has been sent by the new token user.
        // Enter the IDLE state to process the frame.
        state = MSTP_MASTER_STATE_IDLE;
      }
      // RetrySendToken
      else if ((SilenceTimer >= Tusage_timeout) &&
          (RetryCount < Nretry_token))
      {
        RetryCount++;
        // Transmit a Token frame to NS
        SendFrame( FRAME_TYPE_TOKEN, Next_Station, NULL,0);
        EventCount = 0;
        // re-enter the current state to listen for NS
        // to begin using the token.
      }
      // FindNewSuccessor
      else if ((SilenceTimer >= Tusage_timeout) &&
          (RetryCount >= Nretry_token))
      {
        // Assume that NS has failed.
        //Poll_Station = (Next_Station + 1) % (Nmax_master + 1);
	Poll_Station = (Next_Station!=Nmax_master)?Next_Station+1:0;
        // Transmit a Poll For Master frame to PS.
	//if(Poll_Station != This_Station) //don't send it if it's going to us
	SendFrame( FRAME_TYPE_POLL_FOR_MASTER, Poll_Station, NULL,0);
        // no known successor node
        Next_Station = This_Station;
        RetryCount = 0;
        TokenCount = 0;
        EventCount = 0;
        // find a new successor to TS
        state = MSTP_MASTER_STATE_POLL_FOR_MASTER;
      }
      break;
    // The NO_TOKEN state is entered if SilenceTimer becomes greater
    // than Tno_token, indicating that there has been no network activity
    // for that period of time. The timeout is continued to determine
    // whether or not this node may create a token.
    case MSTP_MASTER_STATE_NO_TOKEN:
      // SawFrame
      if ((SilenceTimer < (Tno_token + (Tslot * This_Station))) &&
            (EventCount > Nmin_octets))
      {
        // Some other node exists at a lower address.
        // Enter the IDLE state to receive and process the incoming frame.
        state = MSTP_MASTER_STATE_IDLE;
	//printk(MSTP_MSG "SawFrame\n");
      }
      // GenerateToken
      // FIXME:  (CLB) Neither condition is satisfied here
      // 	       It appears that other implementations ignore the second conditional
      else if ((SilenceTimer >= (Tno_token + (Tslot * This_Station))) && (SilenceTimer < (Tno_token + (Tslot * (This_Station + 2)))))
      {
        // Assume that this node is the lowest numerical address
        // on the network and is empowered to create a token.
        //Poll_Station = (This_Station + 1) % (Nmax_master + 1);
	Poll_Station = (This_Station!=Nmax_master)?This_Station+1:0;
        // Transmit a Poll For Master frame to PS.
	//if(Poll_Station != This_Station) //don't send it if it's going to us
	SendFrame( FRAME_TYPE_POLL_FOR_MASTER, Poll_Station, NULL,0);
        // indicate that the next station is unknown
        Next_Station = This_Station;
        RetryCount = 0;
        TokenCount = 0;
        EventCount = 0;
        // enter the POLL_FOR_MASTER state to find a new successor to TS.
        state = MSTP_MASTER_STATE_POLL_FOR_MASTER;
	//printk(MSTP_MSG "GenerateToken\n");
      }
      break;
    // In the POLL_FOR_MASTER state, the node listens for a reply to
    // a previously sent Poll For Master frame in order to find
    // a successor node.
    case MSTP_MASTER_STATE_POLL_FOR_MASTER:
      // ReceivedReplyToPFM
      if ((ReceivedValidFrame == TRUE) &&
          (DestinationAddress == This_Station) &&
          (FrameType == FRAME_TYPE_REPLY_TO_POLL_FOR_MASTER))
      {
        SoleMaster = FALSE;
        Next_Station = SourceAddress;
        EventCount = 0;
        // Transmit a Token frame to NS
        SendFrame( FRAME_TYPE_TOKEN, Next_Station, NULL,0);
        Poll_Station = This_Station;
        TokenCount = 0;
        RetryCount = 0;
        ReceivedValidFrame = FALSE;
        state = MSTP_MASTER_STATE_PASS_TOKEN;
	//printk(MSTP_MSG "ReceivedReplyToPFM\n");
      }
      // ReceivedUnexpectedFrame
      else if ((ReceivedValidFrame == TRUE) &&
          ((DestinationAddress != This_Station) ||
           (FrameType != FRAME_TYPE_REPLY_TO_POLL_FOR_MASTER)))
      {
        // An unexpected frame was received.
        // This may indicate the presence of multiple tokens.
        ReceivedValidFrame = FALSE;
        // enter the IDLE state to synchronize with the network.
        // This action drops the token.
        state = MSTP_MASTER_STATE_IDLE;
	//printk(MSTP_MSG "ReceivedUnexpectedFrame\n");
      }
      // SoleMaster
      else if ((SoleMaster == TRUE) &&
          ((SilenceTimer >= Tusage_timeout) ||
           (ReceivedInvalidFrame == TRUE)))
      {
        // There was no valid reply to the periodic poll
        // by the sole known master for other masters.
        FrameCount = 0;
        ReceivedInvalidFrame = FALSE;
        state = MSTP_MASTER_STATE_USE_TOKEN;
	//printk(MSTP_MSG "SoleMaster\n");
      }
      // DoneWithPFM
      else if ((SoleMaster == FALSE) &&
          (Next_Station != This_Station) &&
          ((SilenceTimer >= Tusage_timeout) ||
           (ReceivedInvalidFrame == TRUE)))
      {
        // There was no valid reply to the maintenance
        // poll for a master at address PS.
        EventCount = 0;
        // transmit a Token frame to NS
        SendFrame( FRAME_TYPE_TOKEN, Next_Station, NULL,0);
        RetryCount = 0;
        ReceivedInvalidFrame = FALSE;
        state = MSTP_MASTER_STATE_PASS_TOKEN;
	//printk(MSTP_MSG "DoneWithPFM\n");
      }
      // SendNextPFM
      else if ((SoleMaster == FALSE) &&
        (Next_Station == This_Station) && // no known successor node
        ((UINT8)((Poll_Station + 1) % (Nmax_master + 1)) != This_Station) &&
        ((SilenceTimer >= Tusage_timeout) ||
         (ReceivedInvalidFrame == TRUE)))
      {
        Poll_Station =  (Poll_Station + 1) % (Nmax_master + 1);
        // Transmit a Poll For Master frame to PS.
	//if(Poll_Station != This_Station) //don't send it if it's going to us
	SendFrame( FRAME_TYPE_POLL_FOR_MASTER, Poll_Station, NULL,0);
        RetryCount = 0;
        ReceivedInvalidFrame = FALSE;
        // Re-enter the current state.
	//printk(MSTP_MSG "SendNextPFM\n");
      }
      // DeclareSoleMaster
      else if ((SoleMaster == FALSE) &&
          (Next_Station == This_Station) && // no known successor node
          ((UINT8)((Poll_Station + 1) % (Nmax_master + 1)) == This_Station) &&
          ((SilenceTimer >= Tusage_timeout) ||
           (ReceivedInvalidFrame == TRUE)))
      {
        // to indicate that this station is the only master
        SoleMaster = TRUE;
        FrameCount = 0;
        ReceivedInvalidFrame = FALSE;
        state = MSTP_MASTER_STATE_USE_TOKEN;
	//printk(MSTP_MSG "DeclareSoleMaster\n");
      }
      break;
    // The ANSWER_DATA_REQUEST state is entered when a
    // BACnet Data Expecting Reply, a Test_Request, or
    // a proprietary frame that expects a reply is received.
    case MSTP_MASTER_STATE_ANSWER_DATA_REQUEST:
      if (ReplyPostponedTimer <= Treply_delay)
      {
        // Reply
        // If a reply is available from the higher layers
        // within Treply_delay after the reception of the
        // final octet of the requesting frame
        // (the mechanism used to determine this is a local matter),
        // then call SendFrame to transmit the reply frame
        // and enter the IDLE state to wait for the next frame.

        // Test Request
        // If a receiving node can successfully receive and return
        // the information field, it shall do so. If it cannot receive
        // and return the entire information field but can detect
        // the reception of a valid Test_Request frame
        // (for example, by computing the CRC on octets as
        // they are received), then the receiving node shall discard
        // the information field and return a Test_Response containing
        // no information field. If the receiving node cannot detect
        // the valid reception of frames with overlength information fields,
        // then no response shall be returned.
        if (FrameType == FRAME_TYPE_TEST_REQUEST)
        {
          SendFrame( FRAME_TYPE_TEST_RESPONSE, SourceAddress, InputBuffer,Index);
        }
        state = MSTP_MASTER_STATE_IDLE;
      }

      //
      // DeferredReply
      // If no reply will be available from the higher layers
      // within Treply_delay after the reception of the
      // final octet of the requesting frame (the mechanism
      // used to determine this is a local matter),
      // then an immediate reply is not possible.
      // Any reply shall wait until this node receives the token.
      // Call SendFrame to transmit a Reply Postponed frame,
      // and enter the IDLE state.

      else
      {
        SendFrame( FRAME_TYPE_REPLY_POSTPONED, SourceAddress, NULL,0);
        state = MSTP_MASTER_STATE_IDLE;
      }
      break;
    default:
      state = MSTP_MASTER_STATE_IDLE;
      break;
  }

  return;
}

/**************************************************************
 * SendFrame
 **************************************************************
 * UINT8 SendFrameType  --> type of frame to send - see defines
 * UINT8 destination,   --> destination address
 * UINT8 source,        --> source address
 * UINT8 *data,         --> any data to be sent - may be null
 * unsigned data_len    --> number of bytes of data (up to 501)
 **************************************************************/
static void SendFrame( UINT8 SendFrameType,UINT8 destination,UINT8 *data,unsigned data_len)    
{
    UINT8 HeaderCRC; // used for running CRC calculation
    union { unsigned short dw; UINT8 db[2]; } u;
    int i;//,lsr;
    int bytes_written = 0;

    if (!mstp_tty || !mstp_tty->driver.write) return; /* no backend */

    // in order to avoid line contention
    do_gettimeofday(&tv1);
    while (SilenceTimer < Tturnaround)
    {
	// wait, yield, or whatever
	do_gettimeofday(&tv2);
	if(((tv2.tv_usec - tv1.tv_usec)/1000) > Tturnaround)
	    break;
    }
    
    // Disable the receiver, and enable the transmit line driver.
    tx_on();

    // Transmit the preamble octets X'55', X'FF'.
    // As each octet is transmitted, set SilenceTimer to zero.
    OutputBuffer[0] = (UINT8)0x55;
    OutputBuffer[1] = (UINT8)0xFF;

    HeaderCRC = (UINT8)0xFF;
    HeaderCRC = CalcHeaderCRC(SendFrameType,HeaderCRC);

    // Transmit the Frame Type, Destination Address, Source Address,
    // and Data Length octets. Accumulate each octet into HeaderCRC.
    // As each octet is transmitted, set SilenceTimer to zero.
    OutputBuffer[2] = SendFrameType;
    SilenceTimer = 0;

    HeaderCRC = CalcHeaderCRC(destination,HeaderCRC);
    OutputBuffer[3] = destination;
    SilenceTimer = 0;

    HeaderCRC = CalcHeaderCRC(This_Station,HeaderCRC);
    OutputBuffer[4] = This_Station;
    SilenceTimer = 0;

    //use a union here to get the MSB and LSB of DataLen
    u.dw = data_len;
    OutputBuffer[5] = u.db[1];
    HeaderCRC = CalcHeaderCRC(u.db[1],HeaderCRC);

    OutputBuffer[6] = u.db[0];
    HeaderCRC = CalcHeaderCRC(u.db[0],HeaderCRC);

    // Transmit the ones-complement of HeaderCRC. Set SilenceTimer to zero.
    OutputBuffer[7] = ~HeaderCRC;
    SilenceTimer = 0;

    OutputBufferSize = 8;

    // If there are data octets, initialize DataCRC to X'FFFF'.
    if(data_len > 0)
    {
	u.dw = 0xFFFF;
	// Transmit any data octets. Accumulate each octet into DataCRC.
	// As each octet is transmitted, set SilenceTimer to zero.
	for(i = 0; i<data_len; i++)
	{
	    u.dw = CalcDataCRC(data[i],u.dw);
	    OutputBuffer[8+i] = data[i];
	    OutputBufferSize++;
	    SilenceTimer = 0;
	}
	// Transmit the ones-complement of DataCRC, least significant octet first.
	// As each octet is transmitted, set SilenceTimer to zero.
	OutputBuffer[8+data_len] = ~u.db[0];
	OutputBufferSize++;
	SilenceTimer = 0;
	OutputBuffer[9+data_len] = ~u.db[1];
	OutputBufferSize++;
	SilenceTimer = 0;
    }

    //Write the output buffer
    bytes_written = mstp_tty->driver.write(mstp_tty,0,OutputBuffer,OutputBufferSize);

    // Wait until the final stop bit of the most significant CRC octet
    // has been transmitted but not more than Tpostdrive.
    // Use do_gettimeofday to get microsecond resolution
    do_gettimeofday(&tv1);
    while ((inb(0x3E8 + UART_LSR) & UART_LSR_TEMT) !=  UART_LSR_TEMT)
    {
	do_gettimeofday(&tv2);
	if(((tv2.tv_usec - tv1.tv_usec)/1000) > Tpostdrive)
	    break;
    }

    // Disable the transmit line driver.
    tx_off();

    return;
}

/*
 * Line discipline methods
 */

/*
* Handle the 'receiver data ready' interrupt.
* This function is called by the 'tty_io' module in the kernel when
* a block of MSTP data has been received, which can now be decapsulated
* and sent on to the receive frame state machine for further processing.
************************************************************************
* Notes:
* This function is called by the low-level tty driver to send
* characters received by the hardware to the line discpline for
* processing.  
* <cp> (char buffer) is a pointer to the buffer of input character received by the device.  
* <fp> (flag buffer) is a pointer to a pointer of flag bytes which indicate whether a character was received with a parity error, etc.
* Possible fp values:  TTY_NORMAL, TTY_BREAK, TTY_PARITY, TTY_FRAME and TTY_OVERRUN
*
*/
static void mstp_receive(struct tty_struct *tty, const unsigned char *cp,
                               char *fp, int count)
{
    DataAvailable = TRUE;
    while(count--)
    {
	if (fp && *fp++) //any error?
	{
	    ReceiveError = TRUE;
	    cp++; //disregard the byte, let the FSM deal with it
	    continue;
	}
	Receive_Frame_FSM(*cp++);
	DataAvailable = TRUE;
    }
    Master_Node_FSM();
}

/* Open and close keep track of the tty involved */

static int mstp_open(struct tty_struct *tty)
{ MOD_INC_USE_COUNT; mstp_tty = tty; return 0; }

static void mstp_close(struct tty_struct *tty)
{ MOD_DEC_USE_COUNT; mstp_tty = NULL; }

/* Other methods are almost irrelevant... */

static void mstp_flush(struct tty_struct *tty)
{ mstp_head = mstp_tail = InputBuffer; }

static int mstp_room(struct tty_struct *tty)
{ 
    return 65536; //always return a larger # to prevent throttling 
}

/* Handles the incoming custom ioctls and send them to N_TTY */
static int mstp_custom_ioctl(unsigned int cmd, unsigned long arg)
{
    //char ch;
    //char* temp = (char*)arg;
    
    int retVal = 0;

    printk(KERN_ERR MSTP_MSG "IOCTL cmd = %d arg = %ld\n",cmd,arg);

    /* First, make sure the command is valid */
    if(_IOC_TYPE(cmd) != MSTP_IOC_MAGIC) retVal = -ENOTTY;
    if(_IOC_NR(cmd) > MSTP_MAX_NR) retVal = -ENOTTY;
    if(_IOC_NR(cmd) < MSTP_MIN_NR) retVal = -ENOTTY;

    //printk(KERN_ERR MSTP_MSG "Custom IOCTL for us\n");

    /* Next, handle the command */
    switch(cmd)
    {
	case MSTP_IOC_SETMAXMASTER:  //we should shutdown here and restart for each of these
	    //get_user(ch, temp);
	    Nmax_master = arg;
	    break;
	case MSTP_IOC_SETMAXINFOFRAMES:
	    //get_user(ch, temp);
	    //Nmax_info_frames = (unsigned)ch;
	    Nmax_info_frames = arg;
	    break;
	case MSTP_IOC_SETMACADDRESS:
	    //get_user(ch, temp);
	    //This_Station = (unsigned)ch;
	    This_Station = arg;
	    break;
	case MSTP_IOC_GETMAXMASTER:
	    //return put_user(Nmax_master,(char*)arg);
	    retVal = Nmax_master;
	    break;
	case MSTP_IOC_GETMAXINFOFRAMES:
	    //return put_user(Nmax_info_frames,(char*)arg);
	    retVal = Nmax_info_frames;
	    break;
	case MSTP_IOC_GETMACADDRESS: 
	    //return put_user(This_Station,(char*)arg);
	    retVal = This_Station;
	    break;
	default:
	    retVal = -ENOIOCTLCMD;
	    break;
    }
    return retVal;
}

/* Handles the incoming tty ioctls and send them to N_TTY */
/* If it's one of our IOCTLs, call mstp_custom_ioctl	  */
static int mstp_ioctl(struct tty_struct * tty, struct file * file, unsigned int cmd, unsigned long arg)
{
    
    int retVal = 0;
    //struct file* fp = file;

    mod_state = STATE_Done;
    del_timer_sync(&mstp_timer);	/* stop the timers after next execution 	*/

    /* First, make sure the command is valid */
    if(_IOC_TYPE(cmd) != MSTP_IOC_MAGIC) retVal = -ENOTTY;
    if(_IOC_NR(cmd) > MSTP_MAX_NR) retVal = -ENOTTY;
    if(_IOC_NR(cmd) < MSTP_MIN_NR) retVal = -ENOTTY;

    //printk(KERN_ERR MSTP_MSG "IOCTL for us\n");

    switch(cmd)
    {
	/* Allow user space to work with the tty port */
	/* Here, we'd define our custom ioctl calls   */
	case TCSETS:
	case TCSETA:
	case TCGETS:
	case TCGETA:
	    retVal = n_tty_ioctl(tty, (struct file *) file, cmd, (unsigned long) arg);
	    break;
	case MSTP_IOC_SETMAXMASTER:  //we should shutdown here and restart for each of these
	case MSTP_IOC_SETMAXINFOFRAMES:
	case MSTP_IOC_SETMACADDRESS:
	case MSTP_IOC_GETMAXMASTER:
	case MSTP_IOC_GETMAXINFOFRAMES:
	case MSTP_IOC_GETMACADDRESS: 
	    retVal = mstp_custom_ioctl(cmd,(unsigned long)arg);
	    break;
	default:
	    retVal = -ENOIOCTLCMD;
	    break;
    }

    mstp_timer.expires = jiffies + MSTP_5MS; /* 10 ms */
    //mstp_timer.expires = jiffies + MSTP_10MS; /* 10 ms */
    mod_state = STATE_Ready;
    add_timer(&mstp_timer);

    return retVal;
}

static struct tty_ldisc mstp_ldisc = {
    magic:           TTY_LDISC_MAGIC,
    name:            "mstp",
    num:             0, /* ??? */
    open:            mstp_open,
    close:           mstp_close,
    flush_buffer:    mstp_flush,
    receive_buf:     mstp_receive,
    receive_room:    mstp_room,
    ioctl:	     mstp_ioctl,	     
    // other fields can remain NULL
    // FIXME:  we need read and write routines
};


//CLB -- This needs to do something else, probably reset the state machines
//	 but this is a start...
void mstp_default(void)
{
    strcpy(mstp_data.value, "Default");
}

/* proc_read - proc_read_mstp
* proc_read_mstp is the callback function that the kernel calls when
* there's a read file operation on the /proc file (for example,
* cat /proc/mstp). The file's data pointer (&mstp_data) is
* passed in the data parameter. You first cast it to the mstp_data_t
* structure. This proc_read function then uses the sprintf function to
* create a string that is pointed to by the page pointer. The function then
* returns the length of page. Because mstp_data->value is set to
* "Default", the command cat /proc/mstp should return
* mstp Default
*/
static int proc_read_mstp(char *page, char **start, off_t off, int count, int *eof, void *data)
{
    int len;

    /* cast the void pointer of data to mstp_data_t*/
    //struct mstp_data_t *mstp_data=(struct mstp_data_t *)data;

    /* use sprintf to fill the page array with a string */
    //printf a beginning newline for console neatness
    len = sprintf(page, "\n%s %s\n", MODULE_NAME,MODULE_VERSION);
    len += sprintf(page + len, "==============================\n");
    len += sprintf(page + len, "SilenceTimer:     %d\n", SilenceTimer);
    len += sprintf(page + len, "Nmax_master:      %d\n", Nmax_master);
    len += sprintf(page + len, "Nmax_info_frames: %d\n", Nmax_info_frames);
    len += sprintf(page + len, "This_Station:     %d\n", This_Station);
    len += sprintf(page + len, "Next_Station:     %d\n", Next_Station);
    len += sprintf(page + len, "Poll_Station:     %d\n", Poll_Station);
    len += sprintf(page + len, "RFSM State:	  %s\n", rfsm_strings[rfsm_state]);
    len += sprintf(page + len, "MNSM State:	  %s\n", mnsm_strings[state]);
    len += sprintf(page + len, "Tno_token:	  %d\n", Tno_token);
    len += sprintf(page + len, "EventCount:	  %d\n", EventCount);
    len += sprintf(page + len, "TokenCount:	  %d\n", TokenCount);
    len += sprintf(page + len, "FrameCount:	  %d\n", FrameCount);
    len += sprintf(page + len, "Npoll:	  	  %d\n", Npoll);
    len += sprintf(page + len, "SoleMaster:  	  %s\n", SoleMaster?"TRUE":"FALSE");

    //printf a ending newline for console neatness
    len += sprintf(page + len, "\n");

    return len;
}

/* proc_write - proc_write_mstp
* proc_write_mstp is the callback function that the kernel calls
* when there's a write file operation on the /proc file, (for example,
* echo test > /proc/mstp). The file's data pointer
* (&mstp_data) is passed in the data parameter. You first cast it to
* the mstp_data_t structure. The page parameter points to the
* incoming data. You use the copy_from_user function to copy the page
* contents to the data->value field. Before you do that, though, you check
* the page length, which is stored in count to ensure that you don't
* overrun the length of data->value. This function then returns the length
* of the data copied.
*/
static int proc_write_mstp(struct file *file, const char *page, unsigned long count, void *data)
{
    int len;

    /* cast the void pointer of data to mstp_data_t*/
    struct mstp_data_t *mstp_data=(struct mstp_data_t *)data;

    /* do a range checking, don't overflow buffers in kernel modules */
    /* buffer overflows are big security holes */
    if(count > HW_LEN)
	len = HW_LEN;
    else
	len = count;

    /* use the copy_from_user function to copy page data to
    *  * to our mstp_data->value */
    if(copy_from_user(mstp_data->value, page, len)) 
    {
	return -EFAULT;
    }

    /* zero terminate mstp_data->value */
    mstp_data->value[len] = '\0';

    return len;
}

/*
 * Module management
 */

static int __init mstp_init(void)
{
    int err;
    /*
     * At module load time, we must register our mouse and line discipline
     */
    err = tty_register_ldisc(N_MOUSE, &mstp_ldisc); /* nobody uses N_MOUSE */
    mstp_head = mstp_tail = InputBuffer;
    if (err) 
    {
	printk(KERN_ERR MSTP_MSG "can't register line discipline\n");
	return err;
    }

    /* Create the proc entry and make it readable and writeable by all - 0666 */
    mstp_file = create_proc_entry("mstp", 0666, NULL);
    if(mstp_file == NULL) 
    {
	printk(KERN_ERR MSTP_MSG "Can't create proc file entry\n");
	return -ENOMEM;
    }

    /* set the default value of our data to Default. This way a read operation on
     * /proc/mstp will return something. 
     */
    strcpy(mstp_data.value, "Default");
    /* Set mstp_file fields */
    mstp_file->data = &mstp_data;
    mstp_file->read_proc = &proc_read_mstp;
    mstp_file->write_proc = &proc_write_mstp;
    mstp_file->owner = THIS_MODULE;

    init_timer(&mstp_timer);
    mstp_timer.function = mstp_10ms_timer_function;
    mstp_timer.expires = jiffies + MSTP_5MS; /* 10 ms */
    //mstp_timer.expires = jiffies + MSTP_10MS; /* 10 ms */
    mod_state = STATE_Ready;
    add_timer(&mstp_timer);
    /* data is filled at runtime */

    /* everything initialized */
    printk(KERN_INFO "%s %s initialized\n",MODULE_NAME, MODULE_VERSION);

    return 0;
}

static void  __exit mstp_unload(void)
{
    mod_state = STATE_Done;		/* indicate we're done				*/
    del_timer_sync(&mstp_timer);	/* stop the timers after next execution 	*/
    tx_off();				/* turn the transmitter off, just to be sure 	*/
    remove_proc_entry("mstp", NULL);    /* remove the proc entry to avoid Bad Things 	*/
    tty_register_ldisc(N_MOUSE, NULL);  /* unregister ourselves 			*/
    printk(KERN_INFO "%s %s unloaded\n",MODULE_NAME, MODULE_VERSION);
}

module_init(mstp_init);
module_exit(mstp_unload);

MODULE_AUTHOR("Coleman Brumley");
MODULE_DESCRIPTION("BACnet MS/TP Serial Line Discipline");
MODULE_LICENSE("LGPL");
EXPORT_NO_SYMBOLS;


