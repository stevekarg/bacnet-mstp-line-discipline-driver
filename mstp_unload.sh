#!/bin/sh

# Simple wrapper to rmmod

module="kirk"
device="kirkmouse"

minor=`cat /proc/misc | awk "\\$2==\"$module\" {print \\$1}"`
rm -f /dev/$device
rmmod $module

exit 0
