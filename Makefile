#
# Makefile (mstp: Kernel InfraRed Keyboard
#
# Copyright (c) 1999,2001 Alessandro Rubini (rubini@linux.it)
#
##########

# Comment/uncomment the following line to enable/disable debugging
#DEBUG = y

# Change it here or specify it on the "make" commandline or in the environment
ifndef KERNELDIR
  KERNELDIR = /home/cbrumley/TechSystemsKernel
endif

INCLUDEDIR = $(KERNELDIR)/include

INSTALL = install -c
INSTALLBIN = $(INSTALL) -m 755
INSTALLLIB = $(INSTALL) -m 644

ifndef PREFIX
  PREFIX = /usr/local
endif

# Extract version number from headers.
VER = $(shell awk -F\" '/RELEASE/ {print $$2}' $(INCLUDEDIR)/linux/version.h)
MODDIR = /lib/modules/$(VER)/misc
BINDIR = $(PREFIX)/bin
INCDIR = $(PREFIX)/include
INFODR = $(PREFIX)/info

############## no more editing...

INSTALLDIRS =  $(MODDIR) $(BINDIR)

TARGET = mstp
DRIVERS = mstp.o
MSTP_OBJS = mstpmain.o 
CLIENTOBJS = mstprun.o mstpdump.o
CLIENTS =  $(CLIENTOBJS:.o=)  
SCRIPTS =
LDFLAGS =

ifneq ($(DEBUG),)
  DEBFLAGS = -O -g -DMSTP_DEBUG # "-O" is needed to expand inlines
else
  DEBFLAGS =  -O3 -fomit-frame-pointer
  LDFLAGS += -s
endif

#CFLAGS = -D_REENTRANT -lpthread -Wall -I$(INCLUDEDIR) -march=i486 -mcpu=i486 -DTSYSTEMS -O2

%.i: %.c
	$(CC) $(CFLAGS) -E $^ -o $@

%.S: %.c
	$(CC) $(CFLAGS) -S $^ -o $@

CFLAGS = -Wall -D__KERNEL__ -DMODULE -I$(INCLUDEDIR) -march=i486 -mcpu=i486 -DTSYSTEMS -O2 $(DEBFLAGS)

# force make depend
ifneq (.depend,$(wildcard .depend))
all: depend
endif

all: $(DRIVERS) $(CLIENTOBJS) $(CLIENTS) TAGS
	make -C doc

mstp.o: $(MSTP_OBJS)
	ld -r $(MSTP_OBJS) -o $@

install: all
	mkdir -p $(INSTALLDIRS)
	install -c $(DRIVERS) $(MODDIR)
	@-depmod -a 2> /dev/null
	install -c $(CLIENTS) $(SCRIPTS) $(BINDIR)
	make -C doc install

clean-with-docs:
	rm -f *.o *.i *.S *~ */*~ */*/*~ core $(CLIENTS) .depend TAGS

clean: clean-with-docs
	make -C doc clean

tar:
	@if [ "x" = "x$(RELEASE)" ]; then \
	    n=`basename \`pwd\``; cd ..; tar cvf - $$n | gzip > $$n.tar.gz; \
	    echo 'you can set a numeric $$(RELEASE) to make a named tar'; \
	else \
	    mkdir ../$(TARGET)-$(RELEASE) || exit 1; \
	    cp -a . ../$(TARGET)-$(RELEASE) && cd .. && \
	      tar --exclude '*/CVS*' --exclude CVS \
	  	-cvzf $(TARGET)-$(RELEASE).tar.gz $(TARGET)-$(RELEASE); \
	fi
	egrep '[^_]VERSION[^_]|VERSION$$' *.[ch] doc/doc.*

distrib: all clean-with-docs tar

fixme:
	grep FIXME *.[ch] Makefile | grep -v Makefile.\*grep

depend dep:
	$(CC) $(CFLAGS) -MM *.c > .depend

DEPEND DEP:
	$(CC) $(CFLAGS) -M  *.c > .depend

# Depend on objects, so we automagically depend on headers as well.
TAGS: $(CLIENTOBJS) $(SRC:.c=.o)
	etags *.[ch]

ifeq (.depend,$(wildcard .depend))
include .depend
endif


